import org.openqa.selenium.chrome.ChromeDriver

System.setProperty("geb.build.reportsDir", "target/geb-reports")
System.setProperty("geb.build.baseUrl", "https://www.nixsolutions.com")

if(System.properties['os.name'].toLowerCase().contains('Windows')){
    System.setProperty("webdriver.chrome.driver", "./BrowserDrivers/chromedriver.exe")
}
else {
    System.setProperty("webdriver.chrome.driver", "./BrowserDrivers/chromedriver")
}

driver = {
    def driver = new ChromeDriver()
    driver.manage().window().maximize()
    return driver
}