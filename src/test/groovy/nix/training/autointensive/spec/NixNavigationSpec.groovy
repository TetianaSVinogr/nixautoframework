package nix.training.autointensive.spec

import geb.spock.GebReportingSpec
import nix.training.autointensive.page.BlogPage
import nix.training.autointensive.page.StartPage

class NixNavigationSpec extends GebReportingSpec {
    def "Navigate to main page"() {
        when:
            to StartPage

        and:
            "Navigate to Blog page"()

        then:
            at BlogPage
    }
}
