package nix.training.autointensive.page

import geb.Page

class StartPage extends Page {
    static content = {
        productLink (wait: true) {$("a", text: "Blog")}
    }

    static at = {
        title == "NIX – Outsourcing Offshore Software Development Company"
    }

    def "Navigate to Blog page"() {
        productLink.click()
    }

}
